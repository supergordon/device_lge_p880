# Board nameing
TARGET_NO_RADIOIMAGE := true
TARGET_BOARD_PLATFORM := tegra
TARGET_BOOTLOADER_BOARD_NAME := x3

# Target arch settings
TARGET_NO_BOOTLOADER := true
TARGET_CPU_ABI := armeabi-v7a
TARGET_CPU_ABI2 := armeabi
TARGET_ARCH_VARIANT := armv7-a-neon
TARGET_ARCH_VARIANT_CPU := cortex-a9
TARGET_CPU_VARIANT := cortex-a9
TARGET_CPU_SMP := true
ARCH_ARM_HAVE_TLS_REGISTER := true
ARCH_ARM_USE_NON_NEON_MEMCPY := true
TARGET_ARCH := arm

BOARD_KERNEL_CMDLINE := 
BOARD_KERNEL_BASE := 0x10000000
BOARD_KERNEL_PAGESIZE := 2048

# fix this up by examining /proc/mtd on a running device
BOARD_BOOTIMAGE_PARTITION_SIZE := 16777216
BOARD_RECOVERYIMAGE_PARTITION_SIZE := 16777216
BOARD_SYSTEMIMAGE_PARTITION_SIZE := 1056859750
BOARD_USERDATAIMAGE_PARTITION_SIZE := 13164074762
BOARD_FLASH_BLOCK_SIZE := 131072
BOARD_HAS_LARGE_FILESYSTEM := true

TARGET_RECOVERY_PRE_COMMAND := "/system/bin/setup-recovery"

# Try to build the kernel
TARGET_KERNEL_SOURCE := kernel/lge/p880
TARGET_KERNEL_CONFIG := cyanogenmod_x3_defconfig
# Keep this as a fallback
#TARGET_PREBUILT_KERNEL := device/lge/p880/kernel

BOARD_HAS_NO_SELECT_BUTTON := true
#TARGET_RECOVERY_FSTAB = device/lge/p880/fstab.x3
#RECOVERY_FSTAB_VERSION = 2 
TARGET_USERIMAGES_USE_EXT4 := true
BOARD_NO_ALLOW_DEQUEUE_CURRENT_BUFFER := true
BOARD_USE_SKIA_LCDTEXT := true

DEVICE_RESOLUTION := 720x1280
RECOVERY_GRAPHICS_USE_LINELENGTH := true
RECOVERY_SDCARD_ON_DATA := true 
TW_INTERNAL_STORAGE_PATH := "/data/media"
TW_INTERNAL_STORAGE_MOUNT_POINT := "data"
TW_EXTERNAL_STORAGE_PATH := "/external_sd"
TW_EXTERNAL_STORAGE_MOUNT_POINT := "external_sd"
TW_BRIGHTNESS_PATH := /sys/devices/platform/tegra-i2c.1/i2c-1/1-0036/leds/lcd-backlight/brightness
TW_MAX_BRIGHTESS := 100 
TW_NO_USB_STORAGE := true
#TW_NO_SCREEN_BLANK := true
TARGET_RECOVERY_PIXEL_FORMAT := "RGBX_8888"
#TARGET_PREBUILT_RECOVERY_KERNEL := /Volumes/Android/cm/out/target/product/x3/obj/KERNEL_OBJ/arch/arm/boot/zImage

TW_DISABLE_TTF := true

BOARD_HAS_NO_SELECT_BUTTON := true

BUILD_MAC_SDK_EXPERIMENTAL := true
BOARD_SKIP_ANDROID_DOC_BUILD := true

TARGET_USE_CUSTOM_LUN_FILE_PATH := /sys/devices/platform/tegra-udc.0/gadget/lun0/file

# MultiROM config. MultiROM also uses parts of TWRP config
MR_INPUT_TYPE := type_a
MR_INIT_DEVICES := device/lge/p880/mr_init_devices.c
MR_DPI := xhdpi
MR_FSTAB := device/lge/p880/twrp.fstab
MR_KEXEC_MEM_MIN := 0x85000000 

HAVE_SELINUX := true
ifeq ($(HAVE_SELINUX),true)

BOARD_SEPOLICY_DIRS := \
    device/lge/p880/selinux

BOARD_SEPOLICY_UNION := \
    file_contexts \
    file.te \
    device.te \
    domain.te

endif

