$(call inherit-product, $(SRC_TARGET_DIR)/product/languages_full.mk)

# The gps config appropriate for this device
$(call inherit-product, device/common/gps/gps_us_supl.mk)

#$(call inherit-product-if-exists, vendor/lge/p880/p880-vendor.mk)

#DEVICE_PACKAGE_OVERLAYS += device/lge/p880/overlay

PRODUCT_TAGS += dalvik.gc.type-precise
$(call inherit-product, frameworks/native/build/phone-xhdpi-1024-dalvik-heap.mk)

# This device is xhdpi
PRODUCT_AAPT_CONFIG := normal hdpi xhdpi
PRODUCT_AAPT_PREF_CONFIG := xhdpi

LOCAL_PATH := device/lge/p880

PRODUCT_PROPERTY_OVERRIDES += \
	ro.secure=0 

## Recovery 

PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/prebuilt/postrecoveryboot.sh:recovery/root/sbin/postrecoveryboot.sh \
    $(LOCAL_PATH)/prebuilt/recovery.sh:recovery/root/sbin/recovery.sh \
    $(LOCAL_PATH)/prebuilt/recovery.sh:system/bin/setup-recovery \
    $(LOCAL_PATH)/prebuilt/rebootrecovery.sh:recovery/root/sbin/rebootrecovery.sh \
    $(LOCAL_PATH)/twrp.fstab:recovery/root/etc/twrp.fstab

## Boot image

PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/init.x3.rc:root/init.x3.rc \
    $(LOCAL_PATH)/ueventd.x3.rc:root/ueventd.x3.rc \
    $(LOCAL_PATH)/fstab.x3:root/fstab.x3 \
    $(LOCAL_PATH)/init.x3.usb.rc:root/init.x3.usb.rc

$(call inherit-product, build/target/product/full.mk)

PRODUCT_NAME := full_p880
PRODUCT_DEVICE := p880
PRODUCT_MODEL := LG-P880
PRODUCT_MANUFACTURER := LGE
